Update: Stevenson screens and fun with additive manufacturing
#############################################################

:date: 2020-09-12 23:51
:category: Practice
:tags: fabrication, instrumentation, weather

I touched briefly on
`Stevenson screens <https://en.wikipedia.org/wiki/Stevenson_screen>`_
in my
`post on hygrometry <{filename}/2020-06-28-notes-on-hygrometry.rst>`_,
mostly with regard to how cool it is that their namesake was the father of the
famed author.
I'd never built one myself, and never planned on doing until I get around to
building a full-on weatherstation like I will, uh, one of these days.
Then I realized: my girlfriend has a cheap wireless indoor/outdoor thermometer,
and the remote sensor can be a bit, uh, touchy:

.. figure:: {static}/images/remote_thermometer_hot.jpg
   :alt: remote thermometer in sunlight displaying temp of 122.4degF

   I promise it was not 122degF when this picture was taken (low 80s, if even).
   Sometimes I think "keep out of direct sunlight" warnings aren't taken
   seriously enough, especially at our altitude.
   (With apologies to any readers unaccustomed to Farenheit units, this
   reading is about 50degC, with ambient temperature below 30).

Things like this are why we have 3D printers, so I decided to see what might be
publicly available as far as enclosure designs and run one off real quick.

It turns out, Thingiverse has 
`many options <https://www.thingiverse.com/search?q=stevenson+screen&type=things&sort=relevant>`_;
I picked
`this one <https://www.thingiverse.com/thing:2970799>`_
somewhat arbitrarily.
This was a pretty cool model and brought me a little farther into the mindset
of 3D printing than I've been in the past.
I know there are some pretty radical things you can do once you get there
|--|
printing individual screw threads or a ball in a cage or an entire planetary
gearset *in situ*
|--|
but so far I've always treated it as just another traditional 
process, proceding from a drawing and using exact dimensions with known
tolerances *etc*.

This was a different experience:
since I didn't really have any specific dimensions to match, I just scaled it
with the dumb slider in my slicing software and ran test prints until it was
about the right size to accomodate the sensor.
Also, the height of the whole assembly isn't fixed, instead you can print as
many of the annular section as you want to make it as tall as you want.
I did that until it looked reasonable, drove a self-drilling plastic screw
into the bulkhead to mount the sensor, found some all-thread and acorn nuts to
tie it all together and called it good.

.. figure:: {static}/images/stevenson_screen_mounted.jpg
   :alt: enclosure mounted to janky backyard fence

   If what you built looks janky, just bolt it to something that looks even
   jankier that you didn't build.
   50/50 which goes first, the enclosure or the fence.

At the end of the day, it was pretty fun building something so
fast-and-(literally-) loose, the sensor works much better, and I think it looks
altogether quite professional hanging there on the back fence.

.. |--| unicode:: U+2014
