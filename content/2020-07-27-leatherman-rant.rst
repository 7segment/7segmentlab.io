WTF, Leatherman?
################

:date: 2020-07-27 00:27
:category: Practice
:tags: rant, tools

A little over a year ago I decided to start carrying a multitool and,
not knowing anything about the product field, picked up a 
`Leatherman Wave+ <https://www.leatherman.com/wave-10.html>`_.
I've been more-or-less completely satisfied with it:
the scissors and wirecutters tend to runout a bit, but they're nonetheless 
quite handy and the screwdriver is *ingenious* and by this point I can hardly
conceive of going back to life without it on my hip.

Now, this blog isn't, nor is meant to become a prepper/EDC lifestyle
accessory review site: there are plenty of those, they do a great job and
even go on to tell you what kind of hunter camo is in fashion this season to
wear at your wedding and stuff.
I digress.
I had an unpleasant experience attempting to service the above-mentioned
tool, and am calling that out as part of what I find to be a disturbing trend.

Last week I took my Wave+ fishing, and it ended up spending an hour or so
rolling around in bilgewater after falling out of my lap after I used it to
unset a hook and, well, you know how these things go.
This isn't the most respectful way to treat a tool, for sure, but it hardly
constitutes severe abuse near what you'd expect to damage a field tool like
this.
When I got back to shore I dried and oiled it but noticed it had picked up some
sediment internally that made the action a little gritty.
No problem, I'll just take it apart and clean it, as I've done with my
pocketknife any number of times.
Sound good?
Sounds good.

Except...

.. figure:: {static}/images/leatherman_top.jpg
   :alt: top of leatherman tool handle showing T10 security torx fastener head

   What the hell is THAT doing there?

There's an old joke about the Soviet economy: "they pretend to pay us and we 
pretend to work."
I've always thought similarly about security fasteners: the manufacturer
pretends they're sufficient to deter any attempt at meddling by the end-user,
while the end-user pretends not to attempt any field service since the
requisite tools are obviously impossible to obtain.

In point of fact, bit sets for these fasteners were one of the first things I
remember e-commerce being AWESOME at providing.
I ordered a set in about 2003
(from an online vendor that *wasn't* Amazon...  how different things were).[ref]
Around the same time, I worked in a physics lab with a Polish woman who
had a set that her boyfriend had *machined himself*.
If that isn't the most Eastern-European thing ever, I don't know what is, but
the point remains: these bit sets are not now, nor have they been remotely 
difficult to obtain since about the turn of the century, at latest.[/ref]

Now, if ownership of a 
`security torx <https://en.wikipedia.org/wiki/Torx#Variants>`_
bit set is the cost of entry to
attempt repair on a modern Leatherman tool, I'm basically OK with that.
Really I am.
It sets a baseline of earnestness and competence:
you've marketed the thing as a fine tool for the distinguishing craftsperson,
and you don't want your warranty department getting bogged down when JimBob
McHarborFreight ends up bending his the wrong way with help of a rusty
screwdriver and a pipe wrench.
Makes perfect sense.
And, in any case, I've got the right driver, right?

Except...

.. figure:: {static}/images/leatherman_bottom.jpg
   :alt: bottom of leatherman tool handle showing T10 security torx fastener
         head

   Double-what-the-hell-is-THAT doing there?

Yeah.
I have *one*.
Because anyone only ever needs *one*.
This is like how anyone only ever needs *one* can opener:
your can-opening needs, such as you ever encounter them, are perfectly met by
owning exactly one of the tool for the job, and no one is dumb enough to try
and design a can that needs two can openers applied simultaneously to work.
Yet there on the other side of the tool, that fastener mates another one with
the same drive type, necessetating a second identical driver.
This is literally the first time in my life I've ever had need of turning
more than one security fastener at the same time.
And it's all because some smartass engineer decided something like:

#) these fasteners just *look* cooler
#) (more cynically) we can drive repair revenue and product attrition through
   by imposing arbitrary hurdles to owner maintenance
#) (most cynically) we can use this to drive[ref]
   No pun intended.
   I considered changing it to "spur," but, same problem.[/ref]
   sales of interchangeable bits

It turns out, (3) doesn't even make sense, since they don't even sell security
bits.
**Way to hold the line there, leatherman, and keep those bits out the hands
of dangerous criminals**.
You know, the types who might try to replace the battery in their own iPod or
turn off the seatbelt warning chime.
I honestly can't relate to the mindset of someone who, owning a tool company,
would *intentionally design and build tools such that they can't be used to
work on themselves*.

I suppose there's a possibility the reasoning aligned with another option:

4) we really don't trust our customers to undertake repair of their own tools

Looking at third-party repair instructions,[ref]
Because third-party instructions are all that seem to exist.
I kind of doubt the existance of any kind of real service manual, maybe because
the
"`User Guide <https://www.leatherman.com/on/demandware.static/-/Sites-master/default/dwa6ae6da3/userGuides/UG_FullSize.pdf>`_"
is just a two-page product brochure covering their entire multitool line in
twelve languages.
Rest assured, though, their main website clocks in at 10MB over 168 requests.
Leatherman: we may not stand behind our products, but we'll glut your
connection with 10MB of FMV lifestyle porn in the attempt to sell you a new one.
Think of it as *multitool-as-a-service*.
[/ref]
there's some medium-complicated stuff in the stack: eccentric bushings and
friction washers, but nothing all that gnarly.
It can't any more complicated the spring-assist on my pocketknife, and that's
something I've torn down to clean enough times to confidently do it
blindfolded by this point.[ref]
OK, maybe the spring-assist is a *little* easier because it uses regular Torx
fasteners because Kershaw are decent human beings.
To argue that's any real advantage begs the question.[/ref]

Again, I'm basically satisfied with the tool in every other way, but this kind
of overt hostility toward what should be their key customer base is the sort
of thing that might have me looking elsewhere when it comes time to replace it.
Remember I'm not a review site, and I wouldn't recommend against buying this
tool for this reason alone.
Just, if you plan on undertaking a major service operation — like cleaning
some grit out of the mechanism — without sending it in for factory service,
remember to factor in the cost of an extra set of security bits to the TCO.
Then you get to be the only person on your block with both a shiny new
Leatherman tool *and* an otherwise completely redundant set of same.

Notes
=====
