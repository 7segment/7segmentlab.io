Reminder: don't read the comments
#################################

:date: 2020-04-02 10:17
:category: Theory
:tags: math, rant

`This gem <https://hackaday.com/2020/04/01/stay-smarter-than-your-smart-speaker/#comment-6233637>`_
of a comment hit Hackaday today, on an article about "smart" speakers:

    [i]n the millions of Americans out there who own these items lie a small
    percentage of above average aptitude for electronics.

Excercise for the reader: assuming there are 20 million Americans who own a
smart speaker, how many would you expect to be of above average aptitude for
electronics?

Bonus question: how many are above average at math?
