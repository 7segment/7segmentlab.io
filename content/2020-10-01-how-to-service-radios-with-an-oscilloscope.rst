How to Service Radios with an Oscilloscope
##########################################

:date: 2020-10-01 21:21
:category: Theory
:tags: instrumentation, tools, books

With this blog being hosted at and titled after my ham callsign, one might
expect there'd be at least a little radio-related content by this point.
I promise there's some coming, but for a stopgap enjoy this cover shot of a
pamphlet I found buried in the depths of a local antique mall:

.. figure:: {static}/images/how_to_service_radios_with_an_oscilloscope.png
   :alt: cover of Sylvania Electric Products Inc. publication "How to Service
         Radios with an Oscilloscope"

   I sure do love the literalness with which technical publications are titled.

I was too overwhelmed with the sensory overload that is shopping at an antique
mall to note down any interesting details, such as the date of publication.
It didn't have a price listed and it seemed like too much trouble to ask, which
is probably good since I really don't need such a volume in my possession.
Still, if there's one interesting takeaway, it's that Sylvania used to make
scopes?
