#####
About
#####

This is a blog over technical topics of personal interest to me, including
programming,
mathematics,
amateur radio,
and hobbyist electronics and mechanical prototyping.
``AD0SK`` is my amateur callsign, the sequential assignment I received upon
passing my `element 4 <http://www.arrl.org/upgrading-to-an-extra-license>`_.
I'm not anticipating the sort of readership volume that would justify enabling
comments for this site, and I'm not big on social media these days, but would
welcome personal correspondence.
Hit that callsign at protonmail.

********
Taxonomy
********

At present, site content is divided between the three categories of:
    - `Theory <../category/theory.html>`_: exploration of mathematical,
      compuataional, scientific, and engineering principles in the abstract.
    - `Practice <../category/practice.html>`_: practical application of these
      principles. Experiments, hobby projects, etc.
    - `Meta <../category/meta.html>`_: metacontent, about the blog, its author,
      or whatever else.

************
Site details
************

Site powered by `Pelican <https://getpelican.com>`_
and hosted on
`gitlab <https://gitlab.com>`_.
`Theme <https://github.com/drewhutchison/dark-penguin>`_
based on
`Blue Penguin <https://github.com/drewhutchison/dark-penguin>`_
by Jody Frankowski.
Typeset in URW Classico and Glacial Indifferece from the awesome 
`fontlibrary.org <https://fontlibrary.org/>`_.
Analytics (such as they are) by `GoatCounter <https://www.goatcounter.com/>`_.

All content original and © the author unless otherwise noted.
Licensed under
`CC BY-NC 4.0 <https://creativecommons.org/licenses/by-nc/4.0/>`_
